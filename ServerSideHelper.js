'use strict';

var URLUtils = require('dw/web/URLUtils');

/**
 * namespace for sampling form helper functions
 */
var ServerSideHelper = function () {};

/**
* Return product asociated with current sample product
*
* @param {Object} sample product
*
* @return {String} product url
*/
ServerSideHelper.getAsociatedWithSampleProductUrl = function (productLineItem, url) {
    var result = url || '#',
        product,
        master,
        id;

    if (empty(productLineItem)) {
        return result;
    }
    product = productLineItem.getProduct();

    if (!empty(product) && !product.isMaster() && require('product').get(product.ID).isSample()) {
        master = product.getMasterProduct();
        if (!empty(master)) {
            id = master.ID.replace(/SA/g, '');
            if (!empty(require('product').get(id).object)) {
                result = URLUtils.abs('Product-Show', 'pid', id);
            }
        }
    }

    return result;
};

/**
* If Custom object exist update it, else create new with data
*
* @param {String} Custom object id
* @param {Object} Custom object data
* @param {String} Id for new Custom object
*
* @return {Boolean} Result work of the function
*/
ServerSideHelper.recordCustomObject = function (objectId, data, id) {
    var result = false,
        ObjectUtils = require('utils/object'),
        CustomObjectMgr = require('dw/object/CustomObjectMgr'),
        customObject;

    if (empty(objectId) || empty(data) || empty(id)) {
        return result;
    }

    customObject = CustomObjectMgr.getCustomObject(objectId, id);

    if (empty(customObject)) {

        Transaction.begin();
        customObject = CustomObjectMgr.createCustomObject(objectId, id);
        if (!empty(customObject)) {
            ObjectUtils.extend(customObject.custom, data);
            Transaction.commit();
            result = true;
        } else {
            result = false;
        }
    } else {
        Transaction.begin();
        ObjectUtils.extend(customObject.custom, data);
        Transaction.commit();
        result = true;
    }

    return result;
};

/**
* Parse JSON string
*
* @param {String} JSON string
*
* @return {Object} Result JSON string parsing
*/
ServerSideHelper.parseJSONString = function (string) {
    var result;

    if (empty(string)) {
        return result;
    }

    try {
        result = JSON.parse(string);
    } catch(e) {
        Logger.error('Error during parse JSON string: {0}', e.message);
    }

    return result;
};

/**
* Conver string to object use specific separator an key names
*
* @param {String} Convertioned string
* @param {String} Separator items in string
*
* @return {String} Object key name
*/
ServerSideHelper.convertStringToObject = function (string, separator, keyName) {
    var result = null,
        i = 0,
        items;

    if (empty(string)) {
        return result;
    }
    separator = separator || '|';
    items = string.split(separator);

    if (items.length) {
        result = {};
        for (i; i < items.length; i++) {
            let key = keyName ? keyName + i : i;
            result[key] = items[i];
        }
    }

    return result;
};

module.exports = ServerSideHelper;
git clone https://goodnessman@bitbucket.org/goodnessman/custom_libs.git