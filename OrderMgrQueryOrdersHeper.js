/**
 * http://prntscr.com/lhfgaa
 * Searches for order instances by defined parameters
 * 
 * @input CurrentWorkflowComponentInstance : Object
 * @output OrdersToExport : dw.util.SeekableIterator
 * @output OrdersToExportCount : Number
 * @output ExportDirectory : dw.io.File
 * @output DoIt : Boolean
 * @output ExportFile : String
 * @output SearchQuery : String
 *
 */
const OrderMgr = require('dw/order/OrderMgr'),
    Order = require('dw/order/Order'),
    Site = require('dw/system/Site'),
    File = require('dw/io/File');

function execute (pdict) {
    const workflow = pdict.CurrentWorkflowComponentInstance;
    const verificatioExport = workflow.getParameterValue('ExportForVerification');

    const statusQuery = normalizeSubQuery({
        ORDER_STATUS_CREATED: workflow.getParameterValue('ORDER_STATUS_CREATED'),
        ORDER_STATUS_NEW: workflow.getParameterValue('ORDER_STATUS_NEW'),
        ORDER_STATUS_OPEN: workflow.getParameterValue('ORDER_STATUS_OPEN'),
        ORDER_STATUS_COMPLETED: workflow.getParameterValue('ORDER_STATUS_COMPLETED'),
        ORDER_STATUS_FAILED: workflow.getParameterValue('ORDER_STATUS_FAILED'),
        ORDER_STATUS_CANCELLED: workflow.getParameterValue('ORDER_STATUS_CANCELLED'),
        ORDER_STATUS_REPLACED: workflow.getParameterValue('ORDER_STATUS_REPLACED'),
    }, 'status', 7);

    const paymentStatusQuery = normalizeSubQuery({
        PAYMENT_STATUS_NOTPAID: workflow.getParameterValue('PAYMENT_STATUS_NOTPAID'),
        PAYMENT_STATUS_PAID: workflow.getParameterValue('PAYMENT_STATUS_PAID'),
        PAYMENT_STATUS_PAID: workflow.getParameterValue('PAYMENT_STATUS_PAID')
    }, 'paymentStatus', 3);

    const shippingStatusQuery = normalizeSubQuery({
        SHIPPING_STATUS_NOTSHIPPED: workflow.getParameterValue('SHIPPING_STATUS_NOTSHIPPED'),
        SHIPPING_STATUS_PARTSHIPPED: workflow.getParameterValue('SHIPPING_STATUS_PARTSHIPPED'),
        SHIPPING_STATUS_SHIPPED: workflow.getParameterValue('SHIPPING_STATUS_SHIPPED')
    }, 'shippingStatus', 3);

    const exportStatusQuery = normalizeSubQuery({
        EXPORT_STATUS_EXPORTED: workflow.getParameterValue('EXPORT_STATUS_EXPORTED'),
        EXPORT_STATUS_FAILED: workflow.getParameterValue('EXPORT_STATUS_FAILED'),
        EXPORT_STATUS_NOTEXPORTED: workflow.getParameterValue('EXPORT_STATUS_NOTEXPORTED'),
        EXPORT_STATUS_READY: workflow.getParameterValue('EXPORT_STATUS_READY')
    }, 'exportStatus', 4);

    const confirmationStatusQuery = normalizeSubQuery({
        CONFIRMATION_STATUS_CONFIRMED: workflow.getParameterValue('CONFIRMATION_STATUS_CONFIRMED'),
        CONFIRMATION_STATUS_NOTCONFIRMED: workflow.getParameterValue('CONFIRMATION_STATUS_NOTCONFIRMED')
    }, 'confirmationStatus', 2);

    const query = normalizeQuery([statusQuery, paymentStatusQuery, shippingStatusQuery, exportStatusQuery, confirmationStatusQuery], verificatioExport);

    const orders = OrderMgr.searchOrders(query, 'creationDate desc');

    const today = new Date();

    pdict.OrdersToExport = orders;
    pdict.OrdersToExportCount = orders.getCount();
    pdict.ExportDirectory = File('IMPEX/src/' + workflow.getParameterValue('FolderPath'))
    pdict.DoIt = pdict.ExportDirectory.mkdirs();
    pdict.ExportFile = workflow.getParameterValue('FolderPath') + '/' + workflow.getParameterValue('FileNamePrefix') + Site.getCurrent().ID + '_' + today.valueOf() + '.xml'

    if (verificatioExport) {
        pdict.SearchQuery = query;
    }

    return PIPELET_NEXT;
}

function normalizeQuery (query, verificatioExport) {
    let result = [];

    for (let i = query.length; i--;) {
        if (query[i]) {
            result.push(query[i]);
        }
    }

    if (verificatioExport) {
        result.push('custom.verificationExportPassed!=true');
    }

    return result.join(' AND ');
}

function normalizeSubQuery (queryObj, parameterName, maxValuesCount) {
    const keys = Object.keys(queryObj);
    let query = [];

    for (let i = keys.length; i--;) {
        if (queryObj[keys[i]]) {
            query.push(parameterName + '=' + Order[keys[i]]);
        }
    }

    if (!query.length || query.length === maxValuesCount) {
        query = null;
    } else {
        query = '(' + query.join(' OR ') + ')';
    }

    return query;
}