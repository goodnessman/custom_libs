(function (app, $) {
	
	var $cache = {};

	/**
	 * Init cache
	 */
	function initializeCache() {
		$cache = {};
	}

	/**
	 * Check if element is in View
	 * @param {JQuery object} DOM element
	 * @return {Boolean}
	 */
	function isScrolledIntoView(elem) {
		var docViewTop = $(window).scrollTop(), docViewBottom = docViewTop + $(window).height(), elemTop = 0, elemBottom = 0;

		if ($(elem).offset()){
			elemTop = $(elem).offset().top;
			elemBottom = elemTop;
		}

		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	}

	/**
	 * Throttle the scroll events (https://www.sitepoint.com/throttle-scroll-events)
	 * @param {Function} Callback
	 * @param {Number} Throttle time
	 * @return {Function}
	 */
	function throttle(fn, wait) {
		var time = Date.now();

		return function() {
			if ((time + wait - Date.now()) < 0) {
				fn();
				time = Date.now();
			}
		};
	}

	/**
	 * Init events for global navigation
	 */
	function initializeEvents() {}
	
	app.components = app.components || {};
	app.components.global = app.components.global || {};
	app.components.global.util = {
		init : function () {
			initializeCache();
			initializeEvents();
		},
		isScrolledIntoView: isScrolledIntoView
	};

}(window.app = window.app || {}, jQuery));